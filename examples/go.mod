module gitee.com/anhui-jicui-intelligent-robot/zmq4/examples

go 1.16

replace gitee.com/anhui-jicui-intelligent-robot/zmq4 => ../

require (
	github.com/google/uuid v1.0.0
	gitee.com/anhui-jicui-intelligent-robot/zmq4 v0.0.0-00010101000000-000000000000
)
